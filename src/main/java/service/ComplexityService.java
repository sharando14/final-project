package service;

import dao.ComplexityDAO;
import entity.Complexity;

import java.util.List;

public class ComplexityService {

    private ComplexityDAO complexityDAO = new ComplexityDAO();

    public List<Complexity> findAll(String lang) {
        return complexityDAO.findAll(lang);
    }
}
