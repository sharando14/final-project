package service;

import dao.ComplexityDAO;
import dao.GradeDAO;
import dao.SubjectDAO;
import dao.TestDAO;
import dto.GradeWithTestName;
import dto.QuestionContentWithAnswer;
import dto.TestSubjectComplexity;
import entity.Test;

import java.util.ArrayList;
import java.util.List;

public class TestService {

    private TestDAO testDAO = new TestDAO();

    private GradeDAO gradeDAO = new GradeDAO();

    public List<Test> findAll(String lang) {
        return testDAO.findAll(lang);
    }

    public List <GradeWithTestName> findByUserID (Long userId){
        List <GradeWithTestName> list = new ArrayList<>();
        gradeDAO.findByUserId(userId).stream().forEach(grade -> list.add(GradeWithTestName
                .builder()
                .result(grade.getResult())
                .testName(testDAO.findById(grade.getTestId()).getName())
                .build()));
        return list;
    }

    public List <TestSubjectComplexity> findAllForChoose(){

        List <TestSubjectComplexity> list = new ArrayList<>();
        TestService testService = new TestService();
        SubjectDAO subjectDAO = new SubjectDAO();
        ComplexityDAO complexityDAO = new ComplexityDAO();

        testService.findAll("en").stream().forEach(test -> list.add(TestSubjectComplexity
                .builder()
                .id(test.getId())
                .name(test.getName())
                .numberOfQuestion(test.getNumberOfQuestions())
                .subject(subjectDAO.findById(test.getSubjectId()).getName())
                .complexity(complexityDAO.findById(test.getComplexityId()).getName())
                .build()));
        return list;
    }
}
