package service;

import dao.SubjectDAO;
import entity.Subject;

import java.util.List;

public class SubjectService {

   private SubjectDAO subjectDAO = new SubjectDAO();

    public List<Subject> findAll(String lang) {
        return subjectDAO.findAll(lang);
    }
}
