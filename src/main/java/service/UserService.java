package service;

import dao.UserDAO;
import entity.User;

import java.util.List;

import static db.manager.QuerySQL.SELECT_FROM_USER_ALL;

public class UserService {

    private UserDAO userDAO = new UserDAO();

    public User findByUsername (String username){
        return userDAO.findByUsername(username).orElse(null);
    }

    public List<User> findAll(){
        return userDAO.findAll(SELECT_FROM_USER_ALL);
    }
}
