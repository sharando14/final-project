package service;

import dao.AnswerDAO;
import dao.QuestionDAO;
import dto.QuestionContentWithAnswer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class QuestionService {
    QuestionDAO questionDAO = new QuestionDAO();
    AnswerDAO answerDAO = new AnswerDAO();

    public List<QuestionContentWithAnswer> findByTestId(Long testId) {
        List <QuestionContentWithAnswer> questionContentWithAnswers = new ArrayList<>();
        questionDAO.findAllTestId(testId).stream().forEach(question -> questionContentWithAnswers.add(QuestionContentWithAnswer
                .builder()
                .content(question.getContent())
                .counterQuestion(question.getCounterQuestion())
                .answerOfQ(answerDAO.findByQuestionId(question.getId()))
                .build()));
        return questionContentWithAnswers;
    }

    public String percent (double numberOfQ, double numberOfA){
        DecimalFormat df = new DecimalFormat("###.##");
        int k = 100;
        double result = (numberOfA * k) / numberOfQ;
        return df.format(result);
    }
}
