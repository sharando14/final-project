package dao;

import dao.DAOI.RoleDAOI;
import db.manager.DBManager;
import entity.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import static db.manager.QuerySQL.SELECT_FROM_ROLES_BY_ROLE;

public class RoleDAO implements RoleDAOI {

    private Connection connection;

    public RoleDAO() {
        this.connection = DBManager.getConnection();
    }

    @Override
    public Optional<Role> findByRole(String role) {
        Role roles = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_FROM_ROLES_BY_ROLE);
            preparedStatement.setString(1, role);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                roles = new Role(
                        resultSet.getLong("id_roles"),
                        resultSet.getString("role"));
            }
            return Optional.ofNullable(roles);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
