package dao;

import dao.DAOI.ComplexityDAOI;
import db.manager.DBManager;
import entity.Complexity;
import entity.Subject;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static db.manager.QuerySQL.*;

public class ComplexityDAO implements ComplexityDAOI {

    Connection connection;

    public ComplexityDAO() {
        this.connection = DBManager.getConnection();
    }


    @Override
    public Complexity findById(Long id) {
        Complexity complexity = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_FROM_COMPLEXITY_WHERE_ID);
            preparedStatement.setLong(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                complexity = new Complexity(
                        resultSet.getLong("id"),
                        resultSet.getString("name"));
            }
            return complexity;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Complexity> findAll(String lang) {
        List<Complexity> complexities = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_ALL_FROM_COMPLEXITY);
            while (resultSet.next()) {
                complexities.add(new Complexity(
                        resultSet.getLong("id"),
                        resultSet.getString("name")
                ));
            }
            return complexities;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Complexity create(Complexity entity) {
        return null;
    }

    @Override
    public Complexity update(Complexity entity, Long aLong) {
        return null;
    }

    @Override
    public void deleteById(Long aLong) {

    }


}
