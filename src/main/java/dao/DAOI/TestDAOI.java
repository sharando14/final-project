package dao.DAOI;

import entity.Test;

public interface TestDAOI extends GeneralDAOI <Test, Long>{

    void deleteByName (String name);

}
