package dao.DAOI;

import entity.User;

import java.util.Optional;

public interface UserDAOI extends GeneralDAOI <User, Long> {

    Optional <User> findByUsername (String username);

}
