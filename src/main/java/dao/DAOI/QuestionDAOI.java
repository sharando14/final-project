package dao.DAOI;

import entity.Question;

public interface QuestionDAOI extends GeneralDAOI <Question, Long>{

    void deleteByTestId(Long testId);
}
