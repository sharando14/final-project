package dao.DAOI;

import entity.Role;

import java.util.Optional;

public interface RoleDAOI {

    Optional<Role> findByRole (String role);

}
