package dao.DAOI;

import java.util.List;

public interface GeneralDAOI <T,ID>{

    T findById(ID id);

    List<T> findAll(String lang);

    T create (T entity);

    T update (T entity, ID id);

    void deleteById(ID id);

}
