package dao.DAOI;

import entity.Answer;

import java.util.List;
import java.util.Optional;

public interface AnswerDAOI extends GeneralDAOI <Answer, Long> {

}
