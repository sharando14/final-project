package dao.DAOI;

import entity.Subject;

import java.util.Optional;

public interface SubjectDAOI extends GeneralDAOI <Subject, Long> {

    Subject findByName (String name);

}
