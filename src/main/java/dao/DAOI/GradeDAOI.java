package dao.DAOI;

import entity.Grade;

import java.util.List;

public interface GradeDAOI extends GeneralDAOI <Grade, Long> {
    List<Grade> findByUserId (Long id);
}
