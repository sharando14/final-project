package servlets;

import dao.UserDAO;
import entity.User;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static db.manager.QuerySQL.SELECT_FROM_USER_ALL_WHERE_STATUS_TRUE;

@WebServlet("/banUser")

public class BanUserServlet extends HttpServlet {

    UserDAO userDAO = new UserDAO();

    List<User> users = new ArrayList<>();

    private static final Logger LOG = Logger.getLogger(BanUserServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("login") != null) {
            request.setAttribute("login", session.getAttribute("login"));
        }
        users = userDAO.findAll(SELECT_FROM_USER_ALL_WHERE_STATUS_TRUE);

        request.setAttribute("users", users);

        request.getRequestDispatcher("ban.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long userId = null;
        userId = Long.parseLong(request.getParameter("userId"));

        userDAO.ban(userId,false);

        response.sendRedirect("/userAdmin");
    }
}
