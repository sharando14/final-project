package servlets;

import dao.QuestionDAO;
import entity.Question;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static servlets.CreateTestServlet.*;

@WebServlet("/createQuestion")
public class CreateQuestionServlet extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(CreateQuestionServlet.class);

    QuestionDAO questionDAO = new QuestionDAO();

    static int numberOfA;

    static List<Question> questions = new ArrayList<>();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("login") != null) {
            request.setAttribute("login", session.getAttribute("login"));
        }

        request.getRequestDispatcher("createQuestion.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("lang", request.getParameter("lang"));

        for (int i = 1; i <= numberOfQ; i++) {
            String content = request.getParameter("content" + i);
            String numberA = request.getParameter("numberA" + i);
            int number = Integer.parseInt(numberA);
            Question question = Question.builder()
                    .content(content)
                    .counterQuestion(i)
                    .numberOfAnswer(number)
                    .testId(testId)
                    .build();
            questionDAO.create(question, testId);
            questions.add(question);
        }
        response.sendRedirect("/createAnswer");
    }
}

