package servlets;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(LogoutServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOG.debug("Logout starts");
        response.setContentType("text/html");
        HttpSession session = request.getSession(false);
        String lang = session.getAttribute("lang").toString();
        if (session != null) {
            LOG.trace("Session " + session.getId() + " is over");
            session.invalidate();
        }
        request.setAttribute("lang", lang);
        request.getRequestDispatcher("home.jsp").forward(request, response);
    }
}
