package servlets;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

    @WebServlet("/testAdmin")
    public class TestAdminServlet extends HttpServlet {

        private static final Logger LOG = Logger.getLogger(TestAdminServlet.class);

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            HttpSession session = req.getSession();
            if (session.getAttribute("login") != null) {
                req.setAttribute("login", session.getAttribute("login"));
            }
            req.getRequestDispatcher("testAdmin.jsp").forward(req, resp);
        }

        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            HttpSession session = req.getSession();
            session.setAttribute("lang", req.getParameter("lang"));
            req.getRequestDispatcher("testAdmin.jsp").forward(req, resp);
        }
    }
