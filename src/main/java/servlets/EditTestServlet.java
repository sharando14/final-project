package servlets;

import dao.TestDAO;
import dto.QuestionContentWithAnswer;
import entity.Answer;
import entity.Test;
import org.apache.log4j.Logger;
import service.QuestionService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/editServlet")
public class EditTestServlet extends HttpServlet {

    private TestDAO testDAO = new TestDAO();

    private QuestionService questionService = new QuestionService();

    private List<QuestionContentWithAnswer> questionContentWithAnswers = null;
    private static final Logger LOG = Logger.getLogger(EditTestServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("login") != null) {
            request.setAttribute("login", session.getAttribute("login"));
        }
        String testStr = session.getAttribute("testId").toString();
        Long testId = Long.parseLong(testStr);

        Test test = testDAO.findById(testId);

        request.setAttribute("name", test.getName());
        request.setAttribute("time", test.getTime());

        questionContentWithAnswers = questionService.findByTestId(testId);
        request.setAttribute("questionContentWithAnswers", questionContentWithAnswers);

        for (QuestionContentWithAnswer questionContentWithAnswer: questionContentWithAnswers){
            for(Answer answer: questionContentWithAnswer.getAnswerOfQ()){
                request.setAttribute("correctAnswer"+ answer.getId(), answer.isCorrectAnswer());
            }
        }
        request.getRequestDispatcher("editTest.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("/testAdmin");
    }
}
