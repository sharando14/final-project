package servlets;

import dao.GradeDAO;
import dao.UserDAO;
import entity.Grade;
import entity.User;
import org.apache.log4j.Logger;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet("/completedTest")
public class CompletedTestServlet extends HttpServlet {

    private GradeDAO gradeDAO = new GradeDAO();

    private final static Logger LOG = Logger.getLogger(ProfileServlet.class);

    private String result = null;

    private User user = new User();

    private UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        if (session.getAttribute("login") != null) {
            request.setAttribute("login", session.getAttribute("login"));
        }

        result = session.getAttribute("result").toString();

        request.getRequestDispatcher("completedTest.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String testStr = session.getAttribute("testId").toString();
        Long testId = Long.parseLong(testStr);

        String username = session.getAttribute("username").toString();

        user = userService.findByUsername(username);

         Grade grade = Grade.builder()
                .result(result)
                 .testId(testId)
                 .userId(user.getId())
                .build();
        gradeDAO.create(grade, testId, user.getId());

        LOG.info(user.getUsername()+ " passed the test " + result +"%");

        response.sendRedirect("/home");
    }
}
