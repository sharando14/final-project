package servlets;

import dao.GradeDAO;
import dao.TestDAO;
import dto.GradeWithTestName;
import entity.Grade;
import entity.User;
import org.apache.log4j.Logger;
import service.TestService;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {

    private final static Logger LOG = Logger.getLogger(ProfileServlet.class);

    UserService userService = new UserService();

    User user = new User();

    List <GradeWithTestName> list = new ArrayList<>();

    TestService testService = new TestService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("login") != null) {
            request.setAttribute("login", session.getAttribute("login"));
        }

        user = userService.findByUsername(session.getAttribute("username").toString());
        list = testService.findByUserID(user.getId());
        request.setAttribute("list", list);

        request.getRequestDispatcher("profile.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);
    }
}
