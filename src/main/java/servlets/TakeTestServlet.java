package servlets;

import dao.TestDAO;
import dto.TestSubjectComplexity;
import entity.Test;
import org.apache.log4j.Logger;
import service.TestService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


@WebServlet("/takeTest")
public class TakeTestServlet extends HttpServlet {

    private final static Logger LOG = Logger.getLogger(ProfileServlet.class);

    TestService testService = new TestService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("login") != null) {
            request.setAttribute("login", session.getAttribute("login"));
        }

        List<TestSubjectComplexity> list = testService.findAllForChoose();
        request.setAttribute("list", list);
        getServletContext().getRequestDispatcher("/takeTest.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("testId", request.getParameter("test_id"));
        response.sendRedirect("/test");
    }
}
