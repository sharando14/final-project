package servlets;

import dao.UserDAO;
import entity.User;
import org.apache.log4j.Logger;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@WebServlet("/editUser")
public class EditUserServlet  extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(HomeServlet.class);

    UserService userService = new UserService();

    List<User> users = new ArrayList<>();

    UserDAO userDAO = new UserDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("login") != null) {
            request.setAttribute("login", session.getAttribute("login"));
        }

        users = userService.findAll();

        request.setAttribute("users", users);

        request.getRequestDispatcher("editUser.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        for(User user: users){
            User entity = User.builder()
                    .username(request.getParameter("username" + user.getId()))
                    .password(request.getParameter("password" + user.getId()))
                    .name(request.getParameter("name" + user.getId()))
                    .surname(request.getParameter("surname" + user.getId()))
                    .build();
            userDAO.update(entity, user.getId());
        }

        response.sendRedirect("/home");
    }
}
