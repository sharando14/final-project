package servlets;

import dao.AnswerDAO;
import entity.Answer;
import entity.Question;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


import static servlets.CreateQuestionServlet.questions;
import static servlets.CreateTestServlet.testId;


@WebServlet("/createAnswer")
public class CreateAnswerServlet extends HttpServlet {
    AnswerDAO answerDAO = new AnswerDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("login") != null) {
            request.setAttribute("login", session.getAttribute("login"));
        }
        request.setAttribute("questions", questions);
        request.getRequestDispatcher("createAnswer.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("lang", request.getParameter("lang"));

        for (Question question : questions) {
            for (int i = 1; i <= question.getNumberOfAnswer(); i++) {
                String variant = request.getParameter("variant" + question.getCounterQuestion() + i);
                String correctAnswer = request.getParameter("correctAnswer" + question.getCounterQuestion() + i);
                if(correctAnswer==null){
                    correctAnswer="false";
                }
                boolean correctAnswerB = Boolean.parseBoolean(correctAnswer);
            Answer answer;
            answer = Answer.builder()
                    .variant(variant)
                    .counterAnswer(i)
                    .correctAnswer(correctAnswerB)
                    .questionId(question.getId())
                    .build();
                answerDAO.create(answer,question.getId(),testId);
            }
        }
        questions.clear();
        response.sendRedirect("/home");
    }
}
