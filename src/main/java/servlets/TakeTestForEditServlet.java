package servlets;

import entity.Test;
import org.apache.log4j.Logger;
import service.TestService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/takeTestForEdit")
public class TakeTestForEditServlet extends HttpServlet {

    TestService testService = new TestService();

    private final static Logger LOG = Logger.getLogger(TakeTestForEditServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        if (session.getAttribute("login") != null) {
            request.setAttribute("login", session.getAttribute("login"));
        }

        List<Test> tests = testService.findAll(session.getAttribute("lang").toString());
        String lang = session.getAttribute("lang").toString();
        request.setAttribute("tests", tests);

        request.getRequestDispatcher("takeTestForEdit.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("testId", request.getParameter("test"));
        response.sendRedirect("/editServlet");
    }
}
