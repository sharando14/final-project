package servlets;

import dao.TestDAO;
import entity.Subject;
import entity.Test;
import entity.Complexity;
import org.apache.log4j.Logger;
import service.ComplexityService;
import service.SubjectService;
import service.TestService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/createTest")
public class CreateTestServlet extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(LoginServlet.class);

    private List<Subject> subjects;

    private SubjectService subjectService = new SubjectService();

    private TestDAO testDAO = new TestDAO();


    private List<Complexity> complexities;

    private ComplexityService complexityService = new ComplexityService();

    static int numberOfQ;
    static Long testId;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("isLogged") == null) {
            response.sendRedirect("/login");
            return;
        }

        subjects = subjectService.findAll(session.getAttribute("lang").toString());
        String lang = session.getAttribute("lang").toString();
        request.setAttribute("subjects", subjects);

        complexities = complexityService.findAll(session.getAttribute("lang").toString());
        request.setAttribute("complexities", complexities);

        request.getRequestDispatcher("createTest.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        String name = request.getParameter("name");

        String subject = request.getParameter("subject");
        Long subId = Long.parseLong(subject);

        String complexity = request.getParameter("complexity");
        Long complexityId = Long.parseLong(complexity);

        String time = request.getParameter("time");
        int timeI = Integer.parseInt(time);

        //numberQ = number of question
        String numberQ = request.getParameter("numberQ");
        int number = Integer.parseInt(numberQ);
        numberOfQ = number;

        session.setAttribute("numberQ", numberQ);
        Test test;
        test = Test.builder()
                .name(name)
                .time(timeI)
                .numberOfQuestions(number)
                .subjectId(subId)
                .complexityId(complexityId)
                .build();
        testDAO.create(test, subId, complexityId);

        testId = test.getId();

        response.sendRedirect("/createQuestion");
    }

}