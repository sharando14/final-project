package servlets;

import dao.TestDAO;
import entity.Test;
import org.apache.log4j.Logger;
import service.TestService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/deleteTest")
public class DeleteTestServlet extends HttpServlet {

    private List<Test> tests;

    private static final Logger LOG = Logger.getLogger(HomeServlet.class);

    TestService testService = new TestService();

    TestDAO testDAO = new TestDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("isLogged") == null) {
            response.sendRedirect("/login");
            return;
        }
        tests = testService.findAll(session.getAttribute("lang").toString());
        String lang = session.getAttribute("lang").toString();
        request.setAttribute("tests", tests);
        getServletContext().getRequestDispatcher("/deleteTest.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("lang", request.getParameter("lang"));

        String test = request.getParameter("test");
        Long testId = Long.parseLong(test);

        testDAO.deleteById(testId);

        response.sendRedirect("/home");
    }
}
