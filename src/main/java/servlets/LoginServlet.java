package servlets;

import entity.RoleName;
import entity.User;
import org.apache.log4j.Logger;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(LoginServlet.class);

    private UserService userService = new UserService();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOG.debug("LoginServlet starts");
        PrintWriter out = response.getWriter();

        String username = request.getParameter("username");
        LOG.trace("Request parameter: username --> " + username);

        String password = request.getParameter("password");
        LOG.trace("Request parameter: password --> " + password);

        if (username == null || password == null || username.isEmpty() || password.isEmpty()) {
            throw new IOException("Login/password cannot be empty");
        }

        User user = userService.findByUsername(username);
        LOG.trace("Found in DB: user --> " + user);

        if (user != null && user.getUsername().equals(username) && user.getPassword().equals(password)) {
            HttpSession session = request.getSession();
            String userRole = user.getRoleId().getRole();
            session.setAttribute("username", username);
            session.setAttribute("name", user.getName());
            session.setAttribute("isLogged", user);
            session.setAttribute("user", user);

            LOG.trace("Set the session attribute: user --> " + user);
            session.setAttribute("userRole", userRole);

            LOG.trace("Set the session attribute: userRole --> " + userRole);
            session.setAttribute("lang", "ru");

            session.setMaxInactiveInterval(30 * 60);
            LOG.info("User " + user + " logged as " + userRole.toLowerCase());
            if (user.getRoleId().getRole().equals(RoleName.ADMIN.getName())) {
                session.setAttribute("isAdmin", RoleName.ADMIN.getName());
                LOG.trace("Set the session attribute: isAdmin --> true");
            }

            LOG.debug("LoginServlet finished");

            response.sendRedirect("/home");
        } else {
            request.getRequestDispatcher("index.jsp");
            out.print("wrong login or password");
        }
        out.close();

    }
}