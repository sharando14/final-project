package db.manager;

import dto.TestSubjectComplexity;
import service.TestService;
import service.UserService;

import java.util.List;

public class TestSql {

    public static void main(String[] args) {
        TestService testService = new TestService();
        List<TestSubjectComplexity> list = testService.findAllForChoose();
        System.out.println(list);
    }
}