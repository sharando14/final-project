<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="WEB-INF/jspf/directive/lang.jspf" %>
<html lang="${lang}">
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" media="screen" href="style/style.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
Name: <input type="text" value="${name}"><br>
Time: <input type="number" min="5" max="240" step="5" value="${time}"> (min = 5 minutes, max = 240 minutes)<br>
<form id="question_test" action="test" method="post">
    <c:forEach var="q" items="${questionContentWithAnswers}">
        <c:out value="${q.getCounterQuestion()}"/>
        <input name="name${q.getCounterQuestion()}" type="text" value="${q.getContent()}"><br>
        <c:forEach var="a" items="${q.getAnswerOfQ()}">
            <input type="checkbox" name="correctAnswer${a.getId()}" value="${a.isCorrectAnswer()}">
            <input type="text" name="variant${a.getCounterAnswer()}" value="true"><br>
        </c:forEach><br><br>
    </c:forEach>
    <input type="submit" value="Confirm" >
</form>
</body>
</html>
