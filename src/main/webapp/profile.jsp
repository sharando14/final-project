<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="WEB-INF/jspf/directive/lang.jspf" %>

<html lang="${lang}">
<head>
    <title>Profile</title>
    <link rel="stylesheet" type="text/css" media="screen" href="style/style.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
<body>
<fmt:message key="profile_jsp.username"/>: <c:out value="${username}"/><br>
<fmt:message key="profile_jsp.name"/>: <c:out value="${name}"/> <br>
<fmt:message key="profile_jsp.list"/>:<br>
<c:forEach var="l" items="${list}">
    <fmt:message key="profile_jsp.test_name"/>: <c:out value="${l.getTestName()}"/> ||
    <fmt:message key="profile_jsp.result"/>: <c:out value="${l.getResult()}"/>%<br>
</c:forEach>
</body>
</body>
</html>
