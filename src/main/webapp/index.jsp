<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="WEB-INF/jspf/directive/lang.jspf" %>

<html lang="${lang}">
<head>
    <title>Home Page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="style/style.css"/>
    <style>
        body{
            background-image: url(images/thumb-1920-83067.jpg)
        }
    </style>
</head>
<body>
<a href="/home" class="start">Start</a>
</body>
</html>
